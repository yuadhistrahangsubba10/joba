<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <style>
    #slider {
      position: relative;
      width: 50%;
      height: 10vw;
      margin: 10px;
      font-family: 'Helvetica Neue', sans-serif;
      perspective: 1400px;
      transform-style: preserve-3d;
    }

    input[type=radio] {
      position: relative;
      top: 108%;
      left: 50%;
      width: 18px;
      height: 18px;
      margin: 0 15px 0 0;
      opacity: 0.4;
      transform: translateX(-56px);
      cursor: pointer;
    }

    input[type=radio]:nth-child(4) {
      margin-right: 0px;
    }

    input[type=radio]:checked {
      opacity: 1;
    }

    #slider label {
      position: absolute;
      width: 100%;
      height: 100%;
      left: 0;
      top: 0;
      color: white;
      font-size: 70px;
      font-weight: bold;
      border-radius: 5px;
      cursor: pointer;
      display: flex;
      align-items: center;
      justify-content: center;
      transition: transform 400ms ease;
    }

    #slide1 {
      background: red;
    }

    #slide2 {
      background: yellowgreen;
    }

    #slide3 {
      background: dodgerblue;
    }

    /* Slider Functionality */

    /* Active Slide */
    #s1:checked ~ #slide1,
    #s2:checked ~ #slide2,
    #s3:checked ~ #slide3 {
      box-shadow: 0 13px 26px rgba(0, 0, 0, 0.3), 0 12px 6px rgba(0, 0, 0, 0.2);
      transform: translate3d(0%, 0, 0px);
    }

    /* Next Slide */
    #s1:checked ~ #slide2,
    #s2:checked ~ #slide3,
    #s3:checked ~ #slide1 {
      box-shadow: 0 6px 10px rgba(0, 0, 0, 0.3), 0 2px 2px rgba(0, 0, 0, 0.2);
      transform: translate3d(33%, 0, -100px);
    }

    /* Previous Slide */
    #s1:checked ~ #slide3,
    #s2:checked ~ #slide1,
    #s3:checked ~ #slide2 {
      box-shadow: 0 6px 10px rgba(0, 0, 0, 0.3), 0 2px 2px rgba(0, 0, 0, 0.2);
      transform: translate3d(-33%, 0, -100px);
    }

  </style>
</head>
<body>

<section id="slider">
  <input type="radio" name="slider" id="s1" checked>
  <input type="radio" name="slider" id="s2">
  <input type="radio" name="slider" id="s3">

  <label for="s1" id="slide1">1</label>
  <label for="s2" id="slide2">2</label>
  <label for="s3" id="slide3">3</label>
</section>


<script>
  const radioButtons = document.querySelectorAll('input[type=radio]');
  let currentSlide = 0;

  const slideShow = () => {
    setInterval(() => {
      currentSlide = (currentSlide + 1) % radioButtons.length;
      radioButtons[currentSlide].checked = true;
    }, 5000);
  }

  slideShow();
</script>

</body>
</html>
