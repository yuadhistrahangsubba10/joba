<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Main</title>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />

        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta3/css/all.min.css" />

        <style>
            html,
            body {
                height:982px;
            }

            body {
                background-image: url("../img/login.png");
                background-size: cover;
                background-repeat: no-repeat;
                background-attachment: fixed;
                position: relative;

            }

            .blur-background {
                position: absolute;
                top: 0;
                left: 0;
                width: 100%;
                height: 100%;
                background-color: rgba(0, 0, 0, 0.25);
                backdrop-filter: blur(5px);
            }

            @media (max-width: 400px) {
                .blur-background {
                    width: 400px;
                    height:100%;
                }
            }

            .profile {
                display: flex;
                flex-direction: row;
                margin-top: 10%;
                width: 325px;
                margin-bottom: 5%;
            }

            .proText1 {
                font-family: Inter;
                font-size: 9px;
                font-weight: 400;
                line-height: 10.89px;
                text-align: left;
                margin: 0;
                color:#F9B0B0;
            }

            .proText2 {
                font-family: Inter;
                font-size: 14px;
                font-weight: 700;
                line-height: 16.94px;
                text-align: left;
                margin: 0;
                color:#F03848;
            }

            /* Image Slider */
            .sliderContainer {
                display: flex;
                justify-content: center;
                width: 325px;
                height: 166px;
            }

            #slider {
                position: relative;
                width: 100%;
                height: 100%;
                font-family: 'Helvetica Neue', sans-serif;
                perspective: 1400px;
                transform-style: preserve-3d;
                display: flex;
                flex-direction: row;
                justify-content:center;
                border-radius: 15px;
            }

            input[type=radio] {
                position: relative;
                top: 108%;
                left: 20%;
                width: 8px;
                height: 8px;
                margin: 0 15px 0 0;
                opacity: 0.4;
                transform: translateX(-56px);
                cursor: pointer;
                background-color:white;
            }

            input[type=radio]:nth-child(4) {
                margin-right: 0px;
            }

            input[type=radio]:checked {
                opacity: 1;
            }

            #slider label {
                position: absolute;
                width: 100%;
                height: 100%;
                left: 0;
                top: 0;
                color: white;
                font-size: 70px;
                font-weight: bold;
                border-radius: 15px;
                cursor: pointer;
                display: flex;
                align-items: center;
                justify-content: center;
                transition: transform 400ms ease;
            }

            #slide1 {
                background: url("../img/image1.jpg") center/cover;
            }

            #slide2 {
                background: url("../img/image2.jpg") center/cover;
            }

            #slide3 {
                background: url("../img/image3.jpg") center/cover;
            }

            /* Slider Functionality */

            /* Active Slide */
            #s1:checked ~ #slide1,
            #s2:checked ~ #slide2,
            #s3:checked ~ #slide3 {
                box-shadow: 0 13px 26px rgba(0, 0, 0, 0.3), 0 12px 6px rgba(0, 0, 0, 0.2);
                transform: translate3d(0%, 0, 0px);
            }

            /* Next Slide */
            #s1:checked ~ #slide2,
            #s2:checked ~ #slide3,
            #s3:checked ~ #slide1 {
                box-shadow: 0 6px 10px rgba(0, 0, 0, 0.3), 0 2px 2px rgba(0, 0, 0, 0.2);
                transform: translate3d(10%, 0, -100px);
            }

            /* Previous Slide */
            #s1:checked ~ #slide3,
            #s2:checked ~ #slide1,
            #s3:checked ~ #slide2 {
                box-shadow: 0 6px 10px rgba(0, 0, 0, 0.3), 0 2px 2px rgba(0, 0, 0, 0.2);
                transform: translate3d(-10%, 0, -100px);
            }

            .myButton {
                width: 100.99px;
                height:50.87px;
                border-radius: 8px;
                background-color:#2A0406;
                background-color:#2A0406;
                color: #F9B0B0;
                border: 1px solid #EF3547;
                font-size: 12px;
            }

            .balance {
                margin-top: 10%;
                width: 325px;
                height: 12px;
                margin-bottom: 2%;
                display: flex;
                flex-direction: row;
            }

            .balanceText {
                font-family: Inter;
                font-size: 9px;
                font-weight: 700;
                line-height: 10.89px;
                text-align: left;
                color:#F2F2F2;
            }

            /* Image in box */
            .col {
                display: inline-block;
            }

            .gradient-line {
                width: 100%;
                border-width: 0.5px;
                border-style: solid;
                border-image-source: linear-gradient(90deg, rgba(51, 34, 34, 0) 0%, #EF3547 45.01%, rgba(51, 34, 34, 0) 91.87%);
                border-image-slice: 1;
                margin-bottom: 20px
            }

            .pText {
                font-family: 'Inter', sans-serif;
                font-size: 10px;
                font-weight: 500;
                color:#F2F2F2;
            }

            .modalContent {
                width:100%;
                height:100%;
                display:flex;
                justify-content: center;
                color: #F2F2F2;
                align-items: center;
            }

            .buyButton {
                width: 100px;
                height: 38px;
                background: linear-gradient(135deg, #F67E50 100%, #EF3547 100%);
                color: white;
                border-radius: 7px;
                border: none;
                margin-bottom:1rem;
            }

            .plus {
                width: 34px;
                height: 34px;
                gap: 0px;
                border-radius: 6px;
                border: 1px;
            }
            .mainBalance {
                width: 147px;
                height: 34px;
                gap: 0px;
                border-radius: 6px;
                border: 1px;
                background-color: #FFFFFF;
                display: flex;
                justify-content: center;
                align-items: center;

            }
            .spanBalance {
                color: #6E6E6E;
                width: 18px;
                height: 18px;
                display: flex;
                justify-content: center;
                align-items: center;
            }
        </style>
    </head>

    <body>
        <div class="blur-background" style="justify-content: center;">
            <div class="container" style="height: 100%; width: 375px; display: flex; flex-direction: column; align-items: center;">
                <div class="profile" >
                    <img src="../img/profile.png" class="brand_logo" style="border-radius: 50%; height: 26px; width:26px; margin-right:10px" alt="Logo" />
                    <div style="height: 26px">
                        <p class="proText1">Welcome back,</p>
                        <p class="proText2">Ten_tenzin</p>
                    </div>
                </div>

                <div class="sliderContainer">
                    <div id="slider">
                        <input type="radio" name="slider" id="s1" checked>
                        <input type="radio" name="slider" id="s2">
                        <input type="radio" name="slider" id="s3">

                        <label for="s1" id="slide1"><img src="../img/c3.png" alt="Slide 1" style="width:325px;height:166px; border-radius:15px"></label>
                        <label for="s2" id="slide2"><img src="../img/c2.png" alt="Slide 2" style="width:325px;height:166px; border-radius:15px"></label>
                        <label for="s3" id="slide3"><img src="../img/c1.png" alt="Slide 3" style="width:325px;height:166px; border-radius:15px"></label>
                    </div>
                </div>
                <div class="balance">
                    <div style="display: flex; flex-direction: row; height: 12px; width: 50%; align-items: center;">
                        <img src="../img/coin.png" class="brand_logo" style="height: 12px; width:12px; margin-right: 10px;" alt="coin" />
                        <p id="balanceText" class="balanceText" style="margin: 0;">Nu. 10000</p>
                        <span id="iconSpan" style="margin-left: 10px; cursor: pointer; width: 11px; height: 11px; display: flex; align-items: center; justify-content: center;">
                            <i id="toggleIcon" class="fas fa-eye" style="color: #F9B0B0; font-size: 11px;"></i>
                        </span>
                    </div>


                    <div style="display:flex;flex-direction:row;height: 12px; width:50%; justify-content:flex-end;">
                        <img src="../img/ticket.png" class="brand_logo" style="height: 11.14px; width:9px;margin-right:3px;" alt="coin" />
                        <p id="balanceText" class="balanceText" style="margin: 0;">5X</p>
                    </div>

                </div>
                <div class="container" style="width:325px;height:50.87px">
                    <div class="row" style="justify-content: space-between">
                        <button class="myButton" type="submit">Cash-In</button>
                        <button class="myButton" type="submit">Winning</button>
                        <button class="myButton" type="submit" onclick="openModal()">Buy a Tickets</button>
                        {{-- <button class="myButton" type="submit">Buy a Tickets</button> --}}
                    </div>
                </div>

                <!-- The Modal -->
                <div id="myModal" class="modal" style="background: rgba(0, 0, 0, 0.5);">
                    <div class="modalContent">
                      <div class="container" style="width: 279px; height: 248px; display: flex; justify-content: center; flex-direction: column; text-align: center; background: linear-gradient(180deg, rgba(255, 11, 157, 0.8) 0%, rgba(3, 46, 53, 0.8) 100%); border-radius:23px">
                            <div style="display:flex;flex-direction:column;justify-content:center; align-items: center; margin-bottom: 10px">
                                <div style="margin-top: -50px;">
                                    <img src="../img/buy.png" class="brand_logo" style="width: 108.75px; height: 93.22px;" alt="Tickets" />
                                </div>
                            </div>
                            {{-- <span class="close" onclick="closeModal()">&times;</span> --}}
                            <p style="font-family: Inter; font-size: 10px; font-weight: 400; color:#F2F2F2">How many tickets you want to buy?</p>
                            <div class="container" style="display:flex; justify-content:space-between; margin-bottom:10px">
                                <button id="decrementButton" onclick="changeQuantity('decrement')" class="plus">-</button>
                                <div class="mainBalance">
                                    <span id="quantityValue" class="spanBalance">0</span>
                                </div>
                                <button id="incrementButton" onclick="changeQuantity('increment')" class="plus">+</button>
                            </div>
                            <div class="container"  style="display:flex; justify-content:space-between; align-content::center" >
                                <div class="col" style="text-align: left;">
                                    <p style="margin: 0; font-family: Inter; font-size: 10px; font-weight: 400; color:#F2F2F2;">Ticket: 10</p>
                                    <p style="margin: 0; font-family: Inter; font-size: 10px; font-weight: 400; color:#F2F2F2;">Cost/Ticket: 20</p>
                                    <p style="margin: 0; font-family: Inter; font-size: 14px; font-weight: 600; color:#D2FF98; ">Total: Nu. 200</p>
                                </div>
                                <div class="col" style="width:104.22px; height: 72px; align-content:flex-end">
                                    <button class="buyButton" type="submit">Buy</button>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="container" style="margin-top: 10px; margin-bottom:10px; text-align:center">
                    <p style="color: #F2F2F2; font-size:20px; weight:700">Choose a Room</p>
                    <div class="row" style="margin-bottom: 20px">
                        <div class="col" style="position: relative; height: 130px; width:156px;">
                            <img src="../img/roar.png" class="brand_logo" style="height: 130px; width:156px; border-radius: 15px;" alt="Flower" />
                            <p class="centered-text" style=" font-family: 'Inter', sans-serif;position: absolute; top: 50%; left: 50%; transform: translate(-50%, -50%); font-size: 32px; font-weight: 700; text-align: center; background-image: linear-gradient(to right, #FFF6A3 100%, #FF7A00 100%); -webkit-background-clip: text; -webkit-text-fill-color: transparent;">Roar</p>
                        </div>
                        <div class="col" style="position: relative; height: 130px; width:156px;">
                            <img src="../img/lotus.png" class="brand_logo" style="height: 130px; width:156px; border-radius: 15px;" alt="Flower" />
                            <p class="centered-text" style=" font-family: 'Inter', sans-serif;position: absolute; top: 50%; left: 50%; transform: translate(-50%, -50%); font-size: 32px; font-weight: 700; text-align: center; background-image: linear-gradient(to right, #FFA8D1 100%, #FF0B9D 100%); -webkit-background-clip: text; -webkit-text-fill-color: transparent;">Lotus</p>
                        </div>
                    </div>

                    <div class="row" style="margin-bottom: 20px">
                        <div class="col" style="position: relative; height: 130px; width:156px;">
                            <img src="../img/raven.png" class="brand_logo" style="height: 130px; width:156px; border-radius: 15px;" alt="Flower" />
                            <p class="centered-text" style=" font-family: 'Inter', sans-serif;position: absolute; top: 50%; left: 50%; transform: translate(-50%, -50%); font-size: 32px; font-weight: 700; text-align: center; background-image: linear-gradient(to right, #D4F1E6 100%, #091E31 100%); -webkit-background-clip: text; -webkit-text-fill-color: transparent;">Raven</p>
                        </div>
                        <div class="col" style="position: relative; height: 130px; width:156px;">
                            <img src="../img/takin.png" class="brand_logo" style="height: 130px; width:156px; border-radius: 15px;" alt="Flower" />
                            <p class="centered-text" style=" font-family: 'Inter', sans-serif;position: absolute; top: 50%; left: 50%; transform: translate(-50%, -50%); font-size: 32px; font-weight: 700; text-align: center; background-image: linear-gradient(to right, #FFA8D1 100%, #760BFF 100%); -webkit-background-clip: text; -webkit-text-fill-color: transparent;">Takin</p>
                        </div>
                    </div>

                    <div class="row" style="margin-bottom: 20px">
                        <div class="col" style="position: relative; height: 130px; width:156px;">
                            <img src="../img/football.png" class="brand_logo" style="height: 130px; width:156px; border-radius: 15px;" alt="Flower" />
                            <p class="centered-text" style=" font-family: 'Inter', sans-serif;position: absolute; top: 50%; left: 50%; transform: translate(-50%, -50%); font-size: 32px; font-weight: 700; text-align: center; background-image: linear-gradient(to right, #D2FF98 100%, #00FF57 100%); -webkit-background-clip: text; -webkit-text-fill-color: transparent;">Goal</p>
                        </div>
                        <div class="col" style="position: relative; height: 130px; width:156px;">
                            <img src="../img/wild.png" class="brand_logo" style="height: 130px; width:156px; border-radius: 15px;" alt="Flower" />
                            <p class="centered-text" style=" font-family: 'Inter', sans-serif;position: absolute; top: 50%; left: 50%; transform: translate(-50%, -50%); font-size: 32px; font-weight: 700; text-align: center; background-image: linear-gradient(to right, #FFFB98 100%, #FF001F 100%); -webkit-background-clip: text; -webkit-text-fill-color: transparent;">Wild</p>
                        </div>
                    </div>
                </div>

                {{-- Line With Linear Gradient --}}
                <div class="gradient-line"></div>

                {{-- Footer --}}
                <div class="container">
                    <div class="row">
                      <div class="col" style="justify-content:center">
                        <p class="pText">About Us</p>
                      </div>
                      <div class="col">
                        <p class="pText"> Help</p>
                      </div>
                      <div class="col">
                        <p class="pText">Legal</p>
                      </div>
                      <div class="col">
                        <p class="pText">Affilates</p>
                      </div>
                    </div>
                </div>

                <div class="container" style="text-align: center">
                    <p class="pText">All rights reserved EnkiRealm Creatives 2024</p>
                </div>
            </div>
        </div>

        <script>
            const radioButtons = document.querySelectorAll('input[type=radio]');
            let currentSlide = 0;

            const slideShow = () => {
                setInterval(() => {
                    currentSlide = (currentSlide + 1) % radioButtons.length;
                    radioButtons[currentSlide].checked = true;
                }, 5000);
            }

            slideShow();

            function togglePassword(inputId) {
            var input = document.getElementById(inputId);
            var eyeSlash = input.parentElement.querySelector('.toggle-password i');

            if (input.type === 'password') {
                input.type = 'text';
                eyeSlash.classList.remove('fa-eye-slash');
                eyeSlash.classList.add('fa-eye');
            } else {
                input.type = 'password';
                eyeSlash.classList.remove('fa-eye');
                eyeSlash.classList.add('fa-eye-slash');
            }
            }
        </script>

        <script>
            const iconSpan = document.getElementById('iconSpan');
            const balanceText = document.getElementById('balanceText');
            const toggleIcon = document.getElementById('toggleIcon');

            iconSpan.addEventListener('click', function() {
                if (balanceText.style.opacity === '0' || balanceText.textContent === 'XXXXX') {
                    balanceText.style.opacity = '1'; // Show text by setting opacity to 1
                    balanceText.textContent = 'Nu. 10000'; // Reset text content
                    toggleIcon.classList.remove('fa-eye-slash');
                    toggleIcon.classList.add('fa-eye');
                } else {
                    balanceText.style.opacity = '1'; // Hide text by setting opacity to 0
                    balanceText.textContent = 'XXXXX'; // Set text content to XXXXX
                    toggleIcon.classList.remove('fa-eye');
                    toggleIcon.classList.add('fa-eye-slash');
                }
            });
        </script>

        <script>
            // Get the modal
            var modal = document.getElementById("myModal");

            // Get the button that opens the modal
            var btn = document.querySelector(".myButton");

            // Function to open the modal
            function openModal() {
            modal.style.display = "block";
            }

            // Function to close the modal
            function closeModal() {
            modal.style.display = "none";
            }

            // Close the modal if the user clicks outside of it
            window.onclick = function(event) {
            if (event.target == modal) {
                modal.style.display = "none";
            }
            }

            function changeQuantity(action) {
            var valueBox = document.getElementById("quantityValue");
            var currentValue = parseInt(valueBox.textContent);

            if (action === "increment" && currentValue < 1000) {
                valueBox.textContent = currentValue + 10;
            } else if (action === "decrement" && currentValue > 0) {
                valueBox.textContent = currentValue - 10;
            }
            }
        </script>

    </body>
</html>
