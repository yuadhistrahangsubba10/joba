<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Lotus</title>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />

        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta3/css/all.min.css" />

        <style>
            html,
            body {
                height:982px;
            }

            body {
                background-image: url("../img/lotusContainer.png");
                background-size: cover;
                background-repeat: no-repeat;
                background-attachment: fixed;
                position: relative;

            }

            .blur-background {
                position: absolute;
                top: 0;
                left: 0;
                width: 100%;
                height: 100%;
                background-color: rgba(0, 0, 0, 0.25);
                backdrop-filter: blur(5px);
            }

            @media (max-width: 400px) {
                .blur-background {
                    width: 400px;
                    height:100%;
                }
            }

            /* Content */
            .lotusContainer{
                width: 375px;
                height: 269px;
                border: 1px;
                background-image: url("../img/lotus.png");
                background-size: cover;
                background-repeat: no-repeat;
                background-position: center;
            }

            .setting {
                display: flex;
                justify-content: space-between;
                margin-left: 20px;
                margin-right: 20px;
                margin-top: 10%;
            }

            .iconContainer {
                width: 31px;
                height: 31px;
                border-radius: 8px;
                opacity: 0.9px;
                display: flex;
                justify-content: center;
                align-items: center;
                background: #12130F99 60%;
            }

            .icon {
                width: 11px;
                height: 11px;
                border: 1px;
                opacity: 0px;
            }

            .balance {
                width: 325px;
                height: 12px;
                margin-bottom: 2%;
                display: flex;
                flex-direction: row;
            }

            .balanceText {
                font-family: Inter;
                font-size: 9px;
                font-weight: 700;
                line-height: 10.89px;
                text-align: left;
                color:#F2F2F2;
            }

            /* Reveal */
            .f1{
                width: 305px;
                height: 218px;
                border-radius: 15px;
                border: 0.3px;
                background: linear-gradient(180deg, rgba(81, 81, 81, 0.5) 0%, rgba(4, 4, 4, 0.5) 100%);
                position: absolute;
                z-index: 2;
                margin-left:8px;
            }

            .f2{
                width: 303px;
                height: 216px;
                border-radius: 15px;
                border: 0.3px;
                background: linear-gradient(180deg, #003942 0%, #007D7C 100%);
                position: absolute;
                z-index: 1;
                margin-top:80px;
                margin-left:8px;
            }

            .revealContainer{
                background: linear-gradient(180deg, #003942 0%, #007D7C 100%);
                width: 325px;
                height: 258px;
                gap: 0px;
                border-radius: 15px;
                border: 0.3px;
                padding: 10px;
                position: absolute;
                z-index: 3;
                margin-top:20px;
            }

            .revealRow{
                display: flex;
                flex-direction: row;
                justify-content: space-between;
            }

            .revealColumn{
                background: linear-gradient(180deg, #032E35 0%, #053C3C 100%);
                width: 69px;
                height: 61px;
                gap: 0px;
                border-radius: 7px;
                opacity: 0px;
                border: 0.3px solid;
                border-image-source: linear-gradient(180deg, #07605F 0%, #035F65 100%);
                margin: 5px;
                align-content: center
            }

            .textBox {
                display: flex;
                flex-direction: row;
                justify-content: space-between;
                margin-top: 10px;
                margin-left: 5px;
                margin-right: 5px;
            }

            .buyButton {
                width: 186px;
                height: 30.9px;
                background: linear-gradient(135deg, #F67E50 100%, #EF3547 100%);
                color: white;
                border-radius: 8px;
                border: none;
            }


        </style>
    </head>

    <body>
        <div class="blur-background" style="justify-content: center;">
            <div class="container" style="height: 100%; width: 375px; display: flex; flex-direction: column; align-items: center; background: linear-gradient(180deg, #003942 0%, #000000 100%);">
                <div class="lotusContainer">
                    <div class="setting">
                        <div class="iconContainer">
                            <img src="../img/home.png" class="brand_logo" class="icon" alt="Logo" />
                        </div>
                        <div class="iconContainer">
                            <img src="../img/set.png" class="brand_logo" class="icon" alt="Logo" />
                        </div>
                    </div>
                </div>

                {{-- Balance --}}
                <div class="balance">
                    <div style="display: flex; flex-direction: row; height: 12px; width: 50%; align-items: center;">
                        <img src="../img/coin.png" class="brand_logo" style="height: 12px; width:12px; margin-right: 10px;" alt="coin" />
                        <p id="balanceText" class="balanceText" style="margin: 0;">Nu. 10000</p>
                        <span id="iconSpan" style="margin-left: 10px; cursor: pointer; width: 11px; height: 11px; display: flex; align-items: center; justify-content: center;">
                            <i id="toggleIcon" class="fas fa-eye" style="color: #F9B0B0; font-size: 11px;"></i>
                        </span>
                    </div>
                    <div style="display:flex;flex-direction:row;height: 12px; width:50%; justify-content:flex-end;">
                        <img src="../img/ticket.png" class="brand_logo" style="height: 11.14px; width:9px;margin-right:3px;" alt="coin" />
                        <p id="balanceText" class="balanceText" style="margin: 0;">5X</p>
                    </div>
                </div>

                {{-- Reveal --}}
                <div style=" width: 325px; height: 298px;margin-top:10%">
                    <div class="f1">
                    </div>
                    <div class="f2">
                    </div>
                    <div class="revealContainer">
                        <div class="revealRow" >
                          <div class="revealColumn">
                            <p style="font-family: Inter; font-size: 9px; font-weight: 400; text-align: center; color:#F2F2F2; margin:0">Reveal</p>
                          </div>
                          <div class="revealColumn">
                            <p style="font-family: Inter; font-size: 9px; font-weight: 400; text-align: center; color:#F2F2F2; margin:0">Reveal</p>
                          </div>
                          <div class="revealColumn">
                            <p style="font-family: Inter; font-size: 9px; font-weight: 400; text-align: center; color:#F2F2F2; margin:0">Reveal</p>
                          </div>
                          <div class="revealColumn">
                            <p style="font-family: Inter; font-size: 9px; font-weight: 400; text-align: center; color:#F2F2F2; margin:0">Reveal</p>
                          </div>
                        </div>

                        <div class="revealRow" >
                            <div class="revealColumn">
                                <p style="font-family: Inter; font-size: 9px; font-weight: 400; text-align: center; color:#F2F2F2; margin:0">Reveal</p>
                            </div>
                            <div class="revealColumn">
                                <p style="font-family: Inter; font-size: 9px; font-weight: 400; text-align: center; color:#F2F2F2; margin:0">Reveal</p>
                            </div>
                            <div class="revealColumn">
                                <p style="font-family: Inter; font-size: 9px; font-weight: 400; text-align: center; color:#F2F2F2; margin:0">Reveal</p>
                            </div>
                            <div class="revealColumn">
                                <p style="font-family: Inter; font-size: 9px; font-weight: 400; text-align: center; color:#F2F2F2; margin:0">Reveal</p>
                            </div>
                          </div>

                        <div class="revealRow" >
                            <div class="revealColumn">
                                <p style="font-family: Inter; font-size: 9px; font-weight: 400; text-align: center; color:#F2F2F2; margin:0">Reveal</p>
                            </div>
                            <div class="revealColumn">
                                <p style="font-family: Inter; font-size: 9px; font-weight: 400; text-align: center; color:#F2F2F2; margin:0">Reveal</p>
                            </div>
                            <div class="revealColumn">
                                <p style="font-family: Inter; font-size: 9px; font-weight: 400; text-align: center; color:#F2F2F2; margin:0">Reveal</p>
                            </div>
                            <div class="revealColumn">
                                <p style="font-family: Inter; font-size: 9px; font-weight: 400; text-align: center; color:#F2F2F2; margin:0">Reveal</p>
                            </div>
                        </div>
                        <div class="textBox">
                            <p style="font-family: Inter; font-size: 6px; font-weight: 400; text-align: center; color: #FFFFFF;">Card id: 1234567890</p>
                            <p class="centered-text" style=" font-family: 'Inter', sans-serif;position: transform: translate(-50%, -50%); font-size: 9px; font-weight: 700; text-align: right; background-image: linear-gradient(to right, #B0FF94 100%, #FFFDD4 100%); -webkit-background-clip: text; -webkit-text-fill-color: transparent;">
                                Winnings: Nu. 0
                            </p>
                        </div>
                    </div>
                </div>

                {{--            Ok --}}

                    <div style="display: flex; justify-content: space-between; margin-top: 10%; width:325px;">
                        <div style="width: 31px; height: 31px; margin-left:10px; border-radius: 8px; display: flex; justify-content: center; align-items: center; background: #12130F99 60%; border: 1px solid #B0FF94;">
                            <img src="../img/up.png" class="brand_logo" class="icon" alt="Logo" />
                        </div>

                        {{-- <div class="col" style="width:104.22px; height: 72px; align-content:flex-end"> --}}
                            <button class="buyButton" type="submit">Buy</button>
                        {{-- </div> --}}
                        <div style="width: 31px; height: 31px; margin-left:10px; border-radius: 8px; display: flex; justify-content: center; align-items: center; background: #12130F99 60%; border: 1px solid #B0FF94;">
                            <img src="../img/down.png" class="brand_logo" class="icon" alt="Logo" />
                        </div>
                    </div>


                {{-- History --}}
                <div style="width: 325px; height: 205px; margin:5px">
                    <div style="display: flex; flex-direction:row; align-items:center; height: 15px">
                        <div style=" width: 115px; height: 1px; border: 0.5px solid #2B2C2C; "></div>
                        <div style="width: 79px; height: 15px">
                            <p style="font-family: Inter; font-size: 12px; font-weight: 500;text-align: center; color:#FFFFFF;">Game History</p>
                        </div>
                        <div style=" width: 115px; height: 1px; border: 0.5px solid #2B2C2C; "></div>
                    </div>

                    <table class="table table-borderless">
                        <thead>
                          <tr>
                            <th scope="col" style="font-family: Inter; font-size: 10px; font-weight: 600; color: #FFFFFF;">Card id</th>
                            <th scope="col" style="font-family: Inter; font-size: 10px; font-weight: 600; color: #FFFFFF;">Result</th>
                            <th scope="col" style="font-family: Inter; font-size: 10px; font-weight: 600; color: #FFFFFF;">Status</th>
                          </tr>
                        </thead>
                        <div style=" width: 100%; height: 1px; border: 0.5px solid #2B2C2C; "></div>
                        <tbody>
                          <tr>
                            <td style="font-family: Inter; font-size: 10px; font-weight: 400; color:#FFFFFF ">1234567890</td>
                            <td>
                                <p style="font-family: Inter; font-size: 10px; font-weight: 400; color: #B0FF94; margin:0">4 Lotus Flower</p>
                                <p style="font-family: Inter; font-size: 10px; font-weight: 400; color: #707070; margin:0">2 Koi Fish</p>
                                <p style="font-family: Inter; font-size: 10px; font-weight: 400; color: #707070; margin:0">2 Water Lily</p>
                                <p style="font-family: Inter; font-size: 10px; font-weight: 400; color: #707070; margin:0">2 Frog</p>
                                <p style="font-family: Inter; font-size: 10px; font-weight: 400; color: #707070; margin:0">2 Stone</p>
                            </td>
                            <td style="font-family: Inter; font-size: 10px; font-weight: 400; color:#B0FF94"><p>Won</p></td>
                          </tr>
                          <tr>
                            <td style="font-family: Inter; font-size: 10px; font-weight: 400; color:#FFFFFF ">1234567891</td>
                            <td>
                                <p style="font-family: Inter; font-size: 10px; font-weight: 400; color: #707070; margin:0">2 Lotus Flower</p>
                                <p style="font-family: Inter; font-size: 10px; font-weight: 400; color: #707070; margin:0">2 Koi Fish</p>
                                <p style="font-family: Inter; font-size: 10px; font-weight: 400; color: #707070; margin:0">2 Water Lily</p>
                                <p style="font-family: Inter; font-size: 10px; font-weight: 400; color: #707070; margin:0">2 Frog</p>
                                <p style="font-family: Inter; font-size: 10px; font-weight: 400; color: #707070; margin:0">2 Stone</p>
                                <p style="font-family: Inter; font-size: 10px; font-weight: 400; color: #707070; margin:0">2 Grasshopper</p>
                            </td>
                            <td style="font-family: Inter; font-size: 10px; font-weight: 400; color: #F03848"><p>Lost</p></td>
                          </tr>
                        </tbody>
                      </table>

                </div>
            </div>
        </div>
    </body>

    <script>
        const iconSpan = document.getElementById('iconSpan');
        const balanceText = document.getElementById('balanceText');
        const toggleIcon = document.getElementById('toggleIcon');

        iconSpan.addEventListener('click', function() {
            if (balanceText.style.opacity === '0' || balanceText.textContent === 'XXXXX') {
                balanceText.style.opacity = '1'; // Show text by setting opacity to 1
                balanceText.textContent = 'Nu. 10000'; // Reset text content
                toggleIcon.classList.remove('fa-eye-slash');
                toggleIcon.classList.add('fa-eye');
            } else {
                balanceText.style.opacity = '1'; // Hide text by setting opacity to 0
                balanceText.textContent = 'XXXXX'; // Set text content to XXXXX
                toggleIcon.classList.remove('fa-eye');
                toggleIcon.classList.add('fa-eye-slash');
            }
        });
    </script>
</html>
