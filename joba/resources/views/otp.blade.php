<!DOCTYPE html>
<html lang="en">
    <head>
        <title>OTP</title>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />

        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta3/css/all.min.css" />

        <style>
            html,
            body {
                height: 100%;
            }

            body {
                background-image: url("../img/login.png");
                background-size: cover;
                background-repeat: no-repeat;
                background-attachment: fixed;
                position: relative;
            }

            .blur-background {
                position: absolute;
                top: 0;
                left: 0;
                width: 100%;
                height: 100%;
                background-color: rgba(0, 0, 0, 0.25);
                backdrop-filter: blur(10px);
            }

            @media (max-width: 400px) {
                .blur-background {
                    width: 400px;
                    height: 900px;
                }
            }
        </style>
    </head>

    <body>
        <div class="blur-background" style="justify-content: center;">
            <div class="container" style="height: 100%; width: 400px; display: flex; flex-direction: column;">
                <div class="d-flex justify-content-center align-items-center" style="margin-top: 30%;">
                    <img src="../img/logo.png" class="brand_logo" style="max-height: 84px; width: 194px; padding: 10px; justify-content: center; align-items: center; margin-bottom: 20px;" alt="Logo" />
                </div>

                <div class="d-flex justify-content-center align-items-center" style="margin-top: 30%;">
                    <div class="col-8 d-flex justify-content-center align-items-center">
                        <form method="POST" action="{{ route('login') }}">
                            @csrf @if(Session::has('error'))
                            <div class="alert alert-danger" role="alert">
                                {{ Session::get('error') }}
                            </div>
                            @endif

                            <h4 style="color: #f2f2f2; font-size: 36px; weight: 700;">OTP</h4>
                            <p style="color: #f9b0b0; font-size: 10px;">Enter One-Time-PIN sent to your mobile number.</p>

                            <!-- OTP input -->
                            <div class="input-group" style="width: 325px; height: 54px; border-radius: 13px; margin-bottom: 1rem; border: 1px solid #ef3547; padding: 5px; background-color: #2a0406;">
                                <input style="border: none; background-color: #2a0406; color: #f9b0b0;" type="tel" name="mobile" id="mobile" class="form-control" placeholder="OTP" required />
                                <span style="border: none; background-color: #2a0406;" class="input-group-text toggle-password">
                                    <i class="fa fa-check-circle" aria-hidden="true" style="color: #d2ff98;"></i>
                                </span>
                            </div>

                            <button style="margin-right: 10px; width: 89px; height: 54px; border-radius: 13px; margin-bottom: 1rem; border: 1px solid #ef3547; color: #ef3547; background-color: transparent;" onclick="navigateToBack()">
                                Back
                            </button>

                            <button style="width: 221px; height: 54px; background: linear-gradient(135deg, #f67e50, #ef3547); color: white; border-radius: 13px; border: none; margin-bottom: 1rem;" type="submit">Finish Registration</button>

                            <div class="form-group d-flex justify-content-end" style="width: 325px; height: 54px;">
                                <p class="mb-0">
                                    <a style="color: #d2ff98; font-size: 12px; weight: 300;">Resend OTP</a>
                                </p>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <script>
            function navigateToBack() {
                window.location.href = "{{ route('Register') }}";
            }
        </script>
    </body>
</html>
