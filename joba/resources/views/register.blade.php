{{-- <!doctype html>
<html lang="en">

<head>
  <title>Register</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet"
    integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta3/css/all.min.css">

  <style>
    body {
      background-image: url('../img/login.png');
      background-size: cover;
      background-repeat: no-repeat;
      background-attachment: fixed;
    }
  </style>

</head>

<body>
  <main>
    <section>
      <div class="container" style="margin-top: 2rem">
        <div class="row d-flex justify-content-center align-items-center">
          <div class="col-8 d-flex justify-content-center align-items-center">
            <form method="POST" action="{{ route('login') }}">
              @csrf
              @if(Session::has('error'))
              <div class="alert alert-danger" role="alert">
                {{ Session::get('error') }}
              </div>
              @endif
              <div>
                <img src="../img/logo.png" class="brand_logo" style="max-height: 84px; width: 194px; padding: 10px; justify-content: center; align-items: center; margin-top:20px; margin-bottom:20px" alt="Logo">
            </div>

              <h4 style="color:#F2F2F2; font-size: 36px; weight: 700">Register</h4>
              <p style="color:#F9B0B0; font-size: 10px;">Enter your mobile number and password</p>

            <!-- mobile input -->
            <div class="input-group" style="width: 325px; height: 54px; border-radius: 13px; margin-bottom: 1rem; border: 1px solid #EF3547; padding:5px; background-color:#2A0406; ">
                <input style="border: none;background-color:#2A0406; color: #F9B0B0;" type="tel" name="mobile" id="mobile" class="form-control" placeholder="+975 Mobile Number" required />
            </div>

            <!-- username input -->
            <div class="input-group" style="width: 325px; height: 54px; border-radius: 13px; margin-bottom: 1rem; border: 1px solid #EF3547; padding:5px; background-color:#2A0406; ">
                <input style="border: none;background-color:#2A0406; color: #F9B0B0;" type="text" name="username" id="username" class="form-control" placeholder="Username" required />
            </div>


            <!-- Password input -->
            <div class="input-group" style="width: 325px; height: 54px; border-radius: 13px;  margin-bottom: 1rem; border: 1px solid #EF3547; padding:5px; background-color:#2A0406; ">
                <input style="border: none;background-color:#2A0406; color: #F9B0B0;" type="password" name="password" id="password" class="form-control" placeholder="Password" required />
                <span style="border: none; background-color:#2A0406" class="input-group-text toggle-password" onclick="togglePassword('password')" style="background-color: inherit;">
                  <i class="fas fa-eye-slash" style="color: #F9B0B0;"></i>
                </span>
            </div>

            <!-- Confirm Password input -->
            <div class="input-group" style="width: 325px; height: 54px; border-radius: 13px;  margin-bottom: 1rem; border: 1px solid #EF3547; padding:5px; background-color:#2A0406; ">
                <input style="border: none;background-color:#2A0406; color: #F9B0B0;" type="password" name="confirmpassword" id="confirmpassword" class="form-control" placeholder="Confirm Password" required />
                <span style="border: none; background-color:#2A0406" class="input-group-text toggle-password" onclick="togglePassword('confirmpassword')" style="background-color: inherit;">
                  <i class="fas fa-eye-slash" style="color: #F9B0B0;"></i>
                </span>
            </div>

              <!-- Remember Me and Login Button -->
              <button style="width: 325px; height: 54px; background: linear-gradient(135deg, #F67E50, #EF3547); color: white; border-radius: 13px; border: none; margin-bottom:1rem" type="submit">Next</button>

              <div class="form-group d-flex justify-content-center" style="width: 325px; height: 54px;">
                <p class="mb-0">
                  <a style="color: #F9B0B0; font-size: 12px; weight: 300">Already have an account?</a>
                </p>
              </div>

              <button
              style="width: 325px; height: 54px; border-radius: 13px; margin-bottom: 1rem;border: 1px solid #EF3547; color:#EF3547; background-color:transparent"
              onclick="navigateToLogin()">Sign In</button>
            </form>
          </div>
        </div>
      </div>
    </section>
  </main>

  <script src="https://code.jquery.com/jquery-3.7.0.min.js" integrity="sha256-2Pmvv0kuTBOenSvLm6bvfBSSHrUJ+3A7x6P5Ebd07/g=" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-vGy1tE7y9RqewOzPC1BynITbvbHg8Fg9a9jG0g08LnhmrVfyYXbHgETLi1T+jrW3" crossorigin="anonymous"></script>

  <script>
    function togglePassword(inputId) {
      var input = document.getElementById(inputId);
      var eyeSlash = input.parentElement.querySelector('.toggle-password i');

      if (input.type === 'password') {
        input.type = 'text';
        eyeSlash.classList.remove('fa-eye-slash');
        eyeSlash.classList.add('fa-eye');
      } else {
        input.type = 'password';
        eyeSlash.classList.remove('fa-eye');
        eyeSlash.classList.add('fa-eye-slash');
      }
    }

    function navigateToLogin() {
        window.location.href = "{{ route('Login') }}";
    }
  </script>

</body>

</html> --}}

<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Register</title>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />

        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta3/css/all.min.css" />

        <style>
            html,
            body {
                height: 100%;
            }

            body {
                background-image: url("../img/login.png");
                background-size: cover;
                background-repeat: no-repeat;
                background-attachment: fixed;
                position: relative;
            }

            .blur-background {
                position: absolute;
                top: 0;
                left: 0;
                width: 100%;
                height: 100%;
                background-color: rgba(0, 0, 0, 0.25);
                backdrop-filter: blur(10px);
            }

            @media (max-width: 400px) {
                .blur-background {
                    width: 400px;
                    height: 900px;
                }
            }
        </style>
    </head>

    <body>
        <div class="blur-background" style="justify-content: center;">
            <div class="container" style="height: 100%; width: 400px; display: flex; flex-direction: column;">
                <div class="d-flex justify-content-center align-items-center" style="margin-top: 30%;">
                    <img src="../img/logo.png" class="brand_logo" style="max-height: 84px; width: 194px; padding: 10px; justify-content: center; align-items: center; margin-bottom: 20px;" alt="Logo" />
                </div>

                <div class="d-flex justify-content-center align-items-center" style="margin-top: 10%;">
                    <div class="col-8 d-flex justify-content-center align-items-center">
                        <form method="POST" action="{{ route('login') }}">
                            @csrf
                            @if(Session::has('error'))
                            <div class="alert alert-danger" role="alert">
                              {{ Session::get('error') }}
                            </div>
                            @endif

                            <h4 style="color:#F2F2F2; font-size: 36px; weight: 700">Register</h4>
                            <p style="color:#F9B0B0; font-size: 10px;">Enter your mobile number and password</p>

                          <!-- mobile input -->
                          <div class="input-group" style="width: 325px; height: 54px; border-radius: 13px; margin-bottom: 1rem; border: 1px solid #EF3547; padding:5px; background-color:#2A0406; ">
                              <input style="border: none;background-color:#2A0406; color: #F9B0B0;" type="tel" name="mobile" id="mobile" class="form-control" placeholder="+975 Mobile Number" required />
                          </div>

                          <!-- username input -->
                          <div class="input-group" style="width: 325px; height: 54px; border-radius: 13px; margin-bottom: 1rem; border: 1px solid #EF3547; padding:5px; background-color:#2A0406; ">
                              <input style="border: none;background-color:#2A0406; color: #F9B0B0;" type="text" name="username" id="username" class="form-control" placeholder="Username" required />
                          </div>


                          <!-- Password input -->
                          <div class="input-group" style="width: 325px; height: 54px; border-radius: 13px;  margin-bottom: 1rem; border: 1px solid #EF3547; padding:5px; background-color:#2A0406; ">
                              <input style="border: none;background-color:#2A0406; color: #F9B0B0;" type="password" name="password" id="password" class="form-control" placeholder="Password" required />
                              <span style="border: none; background-color:#2A0406" class="input-group-text toggle-password" onclick="togglePassword('password')" style="background-color: inherit;">
                                <i class="fas fa-eye-slash" style="color: #F9B0B0;"></i>
                              </span>
                          </div>

                          <!-- Confirm Password input -->
                          <div class="input-group" style="width: 325px; height: 54px; border-radius: 13px;  margin-bottom: 1rem; border: 1px solid #EF3547; padding:5px; background-color:#2A0406; ">
                              <input style="border: none;background-color:#2A0406; color: #F9B0B0;" type="password" name="confirmpassword" id="confirmpassword" class="form-control" placeholder="Confirm Password" required />
                              <span style="border: none; background-color:#2A0406" class="input-group-text toggle-password" onclick="togglePassword('confirmpassword')" style="background-color: inherit;">
                                <i class="fas fa-eye-slash" style="color: #F9B0B0;"></i>
                              </span>
                          </div>

                            <!-- Remember Me and Login Button -->
                            <button style="width: 325px; height: 54px; background: linear-gradient(135deg, #F67E50, #EF3547); color: white; border-radius: 13px; border: none; margin-bottom:1rem" type="submit">Next</button>

                            <div class="form-group d-flex justify-content-center" style="width: 325px; height: 54px;">
                              <p class="mb-0">
                                <a style="color: #F9B0B0; font-size: 12px; weight: 300">Already have an account?</a>
                              </p>
                            </div>

                            <button
                            style="width: 325px; height: 54px; border-radius: 13px; margin-bottom: 1rem;border: 1px solid #EF3547; color:#EF3547; background-color:transparent"
                            onclick="navigateToLogin()">Sign In</button>
                          </form>
                    </div>
                </div>
            </div>
        </div>

    <script src="https://code.jquery.com/jquery-3.7.0.min.js" integrity="sha256-2Pmvv0kuTBOenSvLm6bvfBSSHrUJ+3A7x6P5Ebd07/g=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-vGy1tE7y9RqewOzPC1BynITbvbHg8Fg9a9jG0g08LnhmrVfyYXbHgETLi1T+jrW3" crossorigin="anonymous"></script>

    <script>
        function togglePassword(inputId) {
        var input = document.getElementById(inputId);
        var eyeSlash = input.parentElement.querySelector('.toggle-password i');

        if (input.type === 'password') {
            input.type = 'text';
            eyeSlash.classList.remove('fa-eye-slash');
            eyeSlash.classList.add('fa-eye');
        } else {
            input.type = 'password';
            eyeSlash.classList.remove('fa-eye');
            eyeSlash.classList.add('fa-eye-slash');
        }
        }

        function navigateToLogin() {
            window.location.href = "{{ route('Login') }}";
        }
    </script>
    
    </body>
</html>

