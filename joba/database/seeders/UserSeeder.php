<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('users')->insert([
            [
                'email' => 'joba@gmail.com',
                'id' => '1',
                'name' => 'joba',
                'password' => bcrypt('joba2024'),
                'phone_number' => '17471711',
            ],
        ]);
    }
}
